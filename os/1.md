# Lesson 1

## General
+ There will be a quiz each lesson due that morning at 0830. **In total, they represent 130 points - more than a GR**.
+ **No** late points for HW. PEXs have a 25% cut for each *day* off. Extensions can be negotiated - just make sure you know it in advance and approach Maj. Brault at least 48 hours ahead of time.

## General-Purpose Computers
+ AnyACK would allow someone to run a batch of punchcards in sequence, printing out its outputs in sequence
+ in the 1960's people began to develop multiprogram operating systems that had rudimentary timesharing.
+ Intel created the 4004 - the first microcontroller - in 1971. It had a 4-bit OS architecture.
+ Windows created its first OS in 1990 - after Mac. Two years later, the first Windows worm came out (WinVIR). Other than being self-replicating, WinVIR was harmless.
+ In the 2000's, the
