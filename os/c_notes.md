# C Notes!

## Structs
+ When referencing a part of a struct with a struct pointer, we need to put the dereference *in paranthesis* (e.g., `(*x).foobar`).
+ **Alternatively (and more easily)**, you can just use the arrow syntax `x->foobar`

## Wait
+ if `NULL` is given to wait as an argument, it will wait for *all* children to complete.
+ if a pointer is provided, the pointer will dereference to the return value of the child, and WAIT will only wait for the next child to finish.
+ The wait function is **what the os tracks** to see if the child has finished. So, if the child returns, but the parent has not entered wait yet, then the child becomes a **zombie process**

