#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "LinkedList.h"

#define SUCCESS 0
#define NULL_INPUT 1
#define OVERFLOW 2
/*
 * Documentation: I used https://stackoverflow.com/questions/4023895/how-to-read-string-entered-by-user-in-c to help
 * with reading a user's input one line at a time - accounting for both overflow and stripping the newline.
 *
 * I also used https://stackoverflow.com/questions/298510/how-to-get-the-current-directory-in-a-c-program to find
 * the current working directory
 *
 * C1C Jordan Stiles helped me greatly with structuring the recall code, as well as the while loop that contains the
 * strtok splitting process.
 */


int terminal_exec(char **args){
    pid_t ret_val;
    int status;

    ret_val = fork();
    if (ret_val !=0) {
        wait(&status);
    }
    else{
        printf("args: ");
        printf("Command: %s Arg1: %s", args[0], args[1]);
        execvp(args[0], args);
    }

    return 0;
}



int readline(char *buffer, size_t size){

    printf("entering readline  ");
    //protected line grab
    int extra;
    int chr;

    if(fgets(buffer, size, stdin) == NULL){
    // No input
        return NULL_INPUT;
    }
    printf("readline received non-NULL line\n");
    // If there's no newline, it means we overflowed.
    // We still want to remove the extra so it doesn't affect future calls.
    if(buffer[strlen(buffer)-1] != '\n'){

        extra=0;
        printf("extra set to zero\n");
        chr = getchar();
        while(( chr != '\n') && (chr != EOF)){
            extra = 1;
            chr = getchar();
        }
        if(extra){
            return OVERFLOW;
        }
        else{
            return SUCCESS;
        }
    }

    //If none of these shennanigans happen, strip the newline and continue
    buffer[strlen(buffer)-1] = '\0';
    return SUCCESS;

}


void cd(char **args, char *cwd){

    int result;
    char * temp;

    // Special use cases first
    if(!strcmp(args[1], "..")){
        //up a directory
        result = chdir("..");
//        printf("chdir call result: %d\n", result);
    }
    else if(!strcmp(args[1], ".")){
        return;
    }
    else if(!strcmp(args[1], "~")){
        result = chdir(getenv("HOME"));
//        printf("chdir call result: %d\n", result);
    }
    else{
        result = chdir(args[1]);
//        printf("chdir call result: %d\n", result);
    }

    //Status Check
    if(result == SUCCESS){
        cwd = getcwd(cwd, sizeof(cwd));
//        printf("New CWD: %s\n", cwd);
    }
}

int main() {

    char input[64], dir[1024];
    char *token;
    char **cmd_args;
    cmd_args = malloc(20 * sizeof(char *));

    int c=0;
    //malloc cmd_args pointers
    for(c=0;c<20;c++){
        cmd_args[c] = (char *)malloc(20 * sizeof(char));
    }

    //Get the CWD
    getcwd(dir, sizeof(dir));
    //Print the prompt
    printf(dir);
    printf(">");
    int status = readline(input, 64);  // First input scan.
    int index, exit=0, i;
    int recall=0;
//    printf("I'm going to create the history list now\n");
    LinkedList *history = linkedListCreate();
//    printf("STATUS: %d\n", status);


    while(!exit) {


        if (!status) {
            // No issues with grabbing inputs
            index = linkedListFindElement(history, input);
            //add the command to history
            if (index == -1) {
//                printf("saving command\n");
                linkedListAppend(history, input);
            } else {
                linkedListDeleteElement(history, index);
                linkedListAppend(history, input);
            }
        }
        else if(status == 1){
            //Do something here.
        }

        token = strtok(input, " \t");
//        printf("Took first token: %s\n", token);
        i = 0;

        while(token != NULL){
            strncpy(cmd_args[i], token, 20);
            token = strtok(NULL, " \t");
            i++;
        }

//        printf("Exited token loop\n");
//        printf("COMMAND: %s FIRST ARG: %s\n",cmd_args[0], cmd_args[1]);

        // Switch for cd, history, and exit
        if(!strcmp(cmd_args[0], "history") && i==1){
            linkedListPrint(history);
        }
        else if(!strcmp(cmd_args[0], "cd")){
            cd(cmd_args, dir);
        }
        else if (!strcmp(cmd_args[0], "recall") && i == 2){
            strncpy(input, linkedListGetElement(history, atoi(cmd_args[1])), sizeof(input));
            recall++;
        }
        else if(!strcmp(cmd_args[0], "exit") && i==1){
            exit++;
        }
        else{
            terminal_exec(cmd_args);
        }
        // Get the new input
        if(recall){
            recall--;
            continue;
        }
        else{
            printf(dir);
            printf(">");
            status=readline(input, 64);
            continue;
        }



    }
    //cleanup
    linkedListDelete(history);



}
