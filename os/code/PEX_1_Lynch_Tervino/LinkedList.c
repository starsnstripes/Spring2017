//
// Created by C19Joanna.Trevino on 1/18/2017.
//
/** pex1.c LinkedList.c
* ===========================================================
* Name: Celeste Trevino 26 Jan. 2017
* Section: T3/4
* Project: <PEX1>
* Purpose: <contains the implementation of a linked list

* ===========================================================
*/

#include "LinkedList.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


/** ----------------------------------------------------------
* <This function switches the values of two nodes>
* @param two nodes that we will switch the values of
* @return none
*/
void swapLinked(Node *value, Node *secondValue){
    char *temp = value->data;
    strncpy(value->data, secondValue->data, sizeof(value->data));
    strncpy(temp, secondValue->data, sizeof(temp));

}
/** ----------------------------------------------------------
* <This function implements a selection sort for my linked list.>
* @param a pointer to our list that we use to sort
* @return none
*/
//void linkedListSelectionSort (LinkedList * list) {
//    Node *firstNode = list->first;
//    int length = list->size - 1;
//    for (int i =0; i <length; i++){
//
//        // Find the node with the smallest element value
//        Node *smallest = firstNode;
//        Node *currentNode = firstNode->next;
//        for (int element = i; element < length; element++){
//            if (currentNode->data < smallest->data)
//            {
//                smallest = currentNode;
//            }
//            currentNode = currentNode->next;
//        }
//        swapLinked(firstNode, smallest);
//        firstNode = firstNode->next;
//    }
//}

/** ----------------------------------------------------------
* <This function creates the linked list and allocates memory for
* our list>
* @param the linked list is a parameter because we need to alloc
* space for it>
* @return <our linked list>
*/
LinkedList * linkedListCreate(){
    LinkedList* list = (LinkedList*) malloc (sizeof(LinkedList));

    list->size = 0;
    list->first = NULL;
    list->last = NULL;

    return list;

}


/** ----------------------------------------------------------
* <This function appends an element at the end of the linked
* list>
* @param linked list gives us our list that we wish to add
* out elemeent to.
* element is what we wish to append>
* @return <none>
*/
void linkedListAppend(LinkedList *list, char *element)
{
    Node* new_item = (Node*)malloc(sizeof(Node));
//    printf("I'm going to copy a string into a new node\n");
    strncpy(new_item->data, element, sizeof(new_item->data));
//    printf("I successfully strncpy'd the data in\n");
    new_item->next = NULL;


    //if the list is empty
    if (list->first == NULL)
    {
//        printf("list-> first was NULL\n");
        list->first = new_item;
        list->last = new_item;
        list->size = list->size + 1;
//        printf("finished allocations\n");
    }
    else
    {
//        printf("list-> first was not NULL\n");
        list ->last->next = new_item;
        list->last = new_item;
        list->size = list->size + 1;
    }
//    printf("EXITING APPEND FUNCT\n");
}

/** ----------------------------------------------------------
* <this function deletes out linkedlist>
* @param linked list - gives us our list to free>
* @return <none>
*/
void linkedListDelete(LinkedList * list)
{
    free(list);
}

/** ----------------------------------------------------------
* <This goes through a list and deletes the element in a position>
* @param our list and a position within our list that we use
* to go through our list and delete the element associated
* with our position>
* @return <none>
*/
void linkedListDeleteElement(LinkedList *list, int position)
{
    if (list->first == NULL)
    {
        return;
    }
    else if(position == 0){
        Node* currentNode = list->first;
        list->first = currentNode->next;
        Node*todelete = currentNode;
        free(todelete);
        list->size -=1;
    }
    else if(position == list->size - 1) {
        Node* currentNode = list->first;
        Node* todelete = list->last;
        for (int i = 0; i<= position -1;i++){
            list->last = currentNode;
        }
        free(todelete);
        list->size -= 1;
    }
    else
    {
        Node* currentNode = list->first;
        for (int i = 0; i < position - 1; i++){
            currentNode = currentNode->next;
        }
        Node* todelete = currentNode->next;
        currentNode->next = currentNode->next->next;
        free(todelete);
        list->size -= 1;
    }
}


/** ----------------------------------------------------------
* <this prints out list out>
* @param our list that we are to go through and print>
* @return <none>
*/
void linkedListPrint(LinkedList *list)
{
    Node* temp = list->first;

    while(temp!= NULL)
    {
        //printf("the value at %p is %d\n", (void *) temp, temp->data);
        printf("%s\t", temp->data);
        temp = temp->next;
    }

}

/** ----------------------------------------------------------
* <The list is used to search through and find the element at
* the position given to the function>
* @param list gives us our  list
* position gives us a position at which we wish to find
* its corresponding element>
* @return <element at given position in the list>
*/
char * linkedListGetElement(LinkedList *list, int position) {
    Node* currentNode = list->first;

    for(int i = 0; i< list->size; i++)
    {
        if(i == position){
            return currentNode->data;
        }
        currentNode = currentNode->next;
    }
}

/** ----------------------------------------------------------
* <This inserts an element in our list and increases the list size>
* @param List: gives us our list that we insert an element into
* position: gives us a position we want to insert our element into
* elementtype value: the value we want to insert>
* @return <none>
*/
void linkedListInsertElement(LinkedList *list, int position, char *element) {
    if (position >= 0 && position <= list->size) {
        Node* newnode = (Node*)malloc(sizeof(Node));
        strncpy(newnode->data, element, sizeof(newnode->data));
        newnode->next = NULL;
        if (position == 0) {
            newnode->next = list->first;
            list->first = newnode;
            list->size += 1;
        }
        else if (position == list->size){
            free(newnode);
            linkedListAppend(list, element);

        }
        else {
            Node *currentNode = list->first;
            for (int i = 0; i < position -1; i++) {
                currentNode = currentNode->next;
            }
            newnode->next = currentNode->next;
            currentNode->next = newnode;
            list->size += 1;
        }
    } else {
        printf("cannot do");
    }
}

/** ----------------------------------------------------------
* <Changes an element value at a specified position>
* @param list: is the list containing the value we want to change
* position: the position we associated with the element value
* we are trying to change
* newValue: the new value we want to change the previous
* element with>
* @return <none>
*/
void linkedListChangeElement(LinkedList *list, int position, char *newValue){
    Node* currentNode = list->first;
    for (int i = 0; i < list->size; i++) {
        if (i == position) {
            strncpy(currentNode->data, newValue, sizeof(currentNode->data));
        }
        currentNode = currentNode->next;
    }
}

/** ----------------------------------------------------------
* <This function finds a specified value's position in a list>
* @param the list: is the list we use to find the value in
* value: the value we want to find the position of>
* @return <none>
*/
int linkedListFindElement(LinkedList *list, char *value) {
    Node *currentNode = list->first;
    for (int i = 0; i < list->size; i++) {
        if (!strcmp(currentNode->data, value)) {
            return i;
        }
        currentNode = currentNode->next;
    }
    return -1;

}
