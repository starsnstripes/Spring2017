#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define BUFFER_SIZE 25
#define READ_END 0
#define WRITE_END 1


int main() {
    char write_msg[BUFFER_SIZE] = "Greetings";
    char read_msg[BUFFER_SIZE];
    pid_t pid;
    int fd[2];

    // Create the pipe (done in the condition)
    if (pipe(fd) == -1){
        fprintf(stderr, "Pipe failed");
        return 1;
    }

    // Fork a child process
    pid = fork();

    if(pid>0){ // <- this means the process is a parent
        // close the unused end of the pipe
        close(fd[READ_END]);

        //write to the pipe
        write(fd[WRITE_END], write_msg, strlen(write_msg) + 1);

        //close the write end of the pipe
        close(fd[WRITE_END]);
    }
    else{ // <- this means the process is the child
        // close the unused end of the pipe
        close(fd[WRITE_END]);

        //read from the pipe
        read(fd[READ_END], read_msg, BUFFER_SIZE);
        printf("Child read: %s\n", read_msg);

        //close the write end of the pipe
        close(fd[READ_END]);

    }

    return 0;
}