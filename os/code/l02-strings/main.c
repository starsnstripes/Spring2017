#include <stdio.h>
#include <string.h>

int main() {
    char *str1 = "Ubuntu rocks!";
    char input[256];

    // Example 1 - print out str1 character by character, strlen
    for (int i = 0; i < strlen(str1); i++) {
        printf("%c\n", str1[i]);
    }

    printf("str1: %s\n", str1);

    // fflush(stdout); This will force the printf above to fully execute

    // Example 2 - fgets
    fgets(input, 256*sizeof(char), stdin);  // get input from standard in, max of 256 characters

    // fgets includes a \n at the end! Get rid of it
    input[strlen(input) - 1] = '\0';    // replace with null terminator

    printf("You entered: %s\n", input); // print it out

    // Example 3 - strcmp - compare two strings
    int retval;

    retval = strcmp(str1, input);

    printf("retval = %d\n", retval);

    // TODO Example 4 - Print out whether the two strings are equal or not

    if(retval == 0){
        printf("The strings are equal!\n");
    }

    

    // Example 5 - strcpy - copy a string into another
    char *str2 = "Clobbered!";

    strncpy(input, str2, 256);  // dest = input, src = str2

    printf("input string is now: %s\n", input);

    return 0;
}