cmake_minimum_required(VERSION 3.9)
project(l10 C)

set(CMAKE_C_STANDARD 99)

add_executable(l10 main.c)

target_link_libraries("-lpthread")