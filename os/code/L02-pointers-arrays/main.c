#include <stdio.h>

int main() {
    // Ex 1 - integer array
    int numbers[] = {34, 35, 36, 37};

    int len_numbers = ((int) sizeof(numbers)) / (int)sizeof(int);

    printf("len of numbers is: %d\n", len_numbers);

    for (int i = 0; i < len_numbers; i++) {
        printf("numbers[%d]: %d\n", i, numbers[i]);
    }

    // Ex 2 - pointers
    int some_int = 42;
    int *ptr_to_int;

    ptr_to_int = &some_int;

    printf("some_int: %d\n", some_int);
    printf("ptr_to_int: %d\n", ptr_to_int);
    printf("*ptr_to_int: %d\n", *ptr_to_int);

    return 0;
}