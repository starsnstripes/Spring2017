#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct t_node {
    char *label;
    struct t_node *left;
    struct t_node *right;
} tree_node;

int equal(tree_node *n1, tree_node *n2) {
    return (strcmp(n1->label, n2->label));
}

int main() {

    // example 1
    struct account {
        int ownerid;
        float balance;
    } myaccount[2];

    struct account *str_ptr = myaccount;

    myaccount[0].ownerid = 0;
    myaccount[0].balance = 10;

    str_ptr[1].balance = 1;
    str_ptr[1].ownerid = 20;

    (*str_ptr).balance = 3;
    (*str_ptr).ownerid = 30;

    printf("myaccount[0].ownerid= %d\n", myaccount[0].ownerid); // is the output what you'd expect?
    printf("myaccount[1].ownerid= %d\n", myaccount[1].ownerid); // is the output what you'd expect?

    str_ptr++;

    str_ptr->balance = 4;   // -> notation
    str_ptr->ownerid = 40;

    printf("myaccount[0].ownerid= %d\n", myaccount[0].ownerid); // is the output what you'd expect?
    printf("myaccount[1].ownerid= %d\n", myaccount[1].ownerid); // is the output what you'd expect?
    

    // Example 2
    tree_node *n1 = NULL;
    tree_node *n2 = NULL;

    n1 = malloc(sizeof(tree_node));
    n1->label = malloc(40 * sizeof(char));
    strcpy(n1->label, "First Node");
    n1->left = NULL;
    n1->right = NULL;

    n2 = malloc(sizeof(tree_node));
    n2->label = malloc(40 * sizeof(char));
    strcpy(n2->label, "Second Node");
    n2->left = NULL;
    n2->right = NULL;

    printf("n1 == n2: %d\n",equal(n1,n2));

    free(n2->label);
    free(n2);

    free(n1->label);
    free(n1);

    return 0;
}