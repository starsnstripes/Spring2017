#include <stdio.h>

int main(int argc, char *argv[]) {
    int ctr;

    printf("Number of arguments: %d\n", argc);

    for (ctr = 0; ctr < argc; ctr++) {
        printf("Arg %d: %s\n", ctr, argv[ctr]);
    }
    
    return 0;
}