#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

int main() {

    // Ex01 - Fork
    pid_t ret_val;
    int x = 0;

    ret_val = fork();
    if (ret_val != 0) {
        // fork returned a non zero value (typically pid of the child)
        // in parent parent runs this code
        pid_t pid_term = wait(&x);  // wait for the child to go first
        printf("In parent...pid of terminated process is %d\n",pid_term);  // print pid terminated proc
        printf("My process ID : %d\n", getpid());  // print my pid
        printf("My parent's ID: %d\n", getppid()); // print parents pid
    } else {
        // fork returned 0 - child runs this code
        printf("In child...\n");
        printf("My process ID : %d\n", getpid());
        printf("My parent's ID: %d\n", getppid());
    }

    printf("You should see this message 2x once from the parent and once from child prior to termination.\n");

    return 0;
}