# Lesson 5

## GNU C Library
+ About 2000 subroutines (*This is where printf lives*)
+ POSIX is a system standard for system calls. If your code is POSIX compliant, then code written with POSIX system calls should work on any other POSIX-compliant architecture.
+ Using the GNU C Library does *not* restrict you from calling the system calls directly
  + Most of the subroutines are **wrapper functions** that handle your data before making the system call. `printf`, for instance, handles formatting before calling the system call that prints to terminal.

## System Call
+ To make a system call, the program ensures that the correct parameters are loaded into the correct registers, and that the system call number is stored. Then, the **program fires an interrupt**.

## The five categories of system calls, with a Windows and then Linux example
1. Process Control: `CreateProcess()` (windows), `fork()` (linux)
2. File Manipulation: `CreateFile()`, `open()`
3. Device Manipulation: `WriteConsole()`, `write()`
4. Information Maintenance: `SetTimer()`, `alarm()`
5. Communication: `CreatePipe()`, `pipe()`
6. Protection: `SetFileSecurity()`, `chmod()`

## Process Creation
+  `fork()` is the UNIX call for creating a process.
+ `pid 1`is the Linux boot process `init`, which will `fork()` a lot of other processes (usually the daemons)
+ Each process has a parent that is tracked by the os. If the parent dies, then the children processes can become orphan processes.

### The `fork()` Process
+ **Each Process has its own process memory.** This means that **each process has a stack, heap, code text, etc.**
+ Each process also has an exact duplicate of its parent in its memory, down to the source code.
+ If a child calls `exec()`, it obliterates all the memory in its process memory then runs the code dictated by parameters.
  + This means that the only way to truly "run" a program is to `fork()` a new one then populate it with `exec()`
+ The `wait` system call will hold up a parent process until its children completes their
+ Remember, if just `fork()` is called, then the child will simply execute the code of the parent.
+ **The child picks up executing code right after the `fork` call!!**

#### Return Values for `fork()`
+ -1 = Failure
+ 0 = The process is a child
+ >0 = The process is a parent

### The `wait()` system call
+ Takes an argument representing the parent process that should wait.
+ The target process will wait for **one** child process to finish before moving on.

### the `ps aux` command
+ Lists the processes
+ pipe into `grep <pid>` to find info on a certain process

### The `exec()` system call
+ Many different versions, to include `execvp`, `execc`, etc.
+ `execvp` takes a string as the first argument and an array as a second one
	+ Typically, it's `argv[0]` and `argv` that get argued (like how the shell would interpret a command)
+ Remember, when `exec()` is called in any variation, **the child process obliterates iteself and replaces its memory with the results of `exec()`**

