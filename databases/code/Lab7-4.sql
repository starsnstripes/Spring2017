################################################################# 
# Everything following # to the end of line is viewed 
# as comments by SQL.  The SQL keywords in the following are 
# capitalized here to make it easier to recognize them and 
# understand the SQL commands.  Note that SQL is case-insensitive 
# so it is fine if you use lower-case or upper case or mixed.
# SQL also supports double hyphens -- as comments like so:
-- This is also a comment
################################################################

################################################################# Create a new blank database and use it
################################################################
-- Optionally use the following line if you want to run all
-- commands at once.
DROP DATABASE IF EXISTS cs364;
CREATE DATABASE cs364;   
USE cs364;

################################################################
# See what tables are currently in that database
################################################################
-- SHOW TABLES;

################################################################# Create a new blank table in the current database
################################################################

CREATE TABLE salesperson (
      snum INTEGER NOT NULL,
      sname CHAR(15),
      city CHAR (15),
      comm NUMERIC(3,2),
      PRIMARY KEY (snum)
);


################################################################# Insert 5 records into the newly created table
################################################################

INSERT INTO salesperson VALUES (1001, 'James', 'London', 0.12);
INSERT INTO salesperson VALUES (1002, 'Peter', 'San Jose', 0.13);
INSERT INTO salesperson VALUES (1004, 'David', 'London', 0.11);
INSERT INTO salesperson VALUES (1007, 'Jeremy', 'Barcelona', 0.15);
INSERT INTO salesperson VALUES (1003, 'Abraham', 'New York', 0.10);

# Check what tables are currently in that database again
-- SHOW TABLES;

# Ask MySQL to describe the structure (i.e. schema) of the table
# DESCRIBE salesperson;
/*
# Show all current records in this table
SELECT * 
     FROM salesperson;
*/
################################################################
# The following is a list of simple SQL queries involving 
# a single table
################################################################
################################################################
# (I) Three simple SQL queries, each with a single relational 
# or string operator in # the where clause
################################################################

#SELECT *
 #   FROM salesperson
  #     WHERE city = 'London';

#SELECT *
 #    FROM salesperson
  #       WHERE city LIKE '%on%';

#SELECT *
 #    FROM salesperson
  #       WHERE comm >= 0.13;

#SELECT * 
 #    FROM salesperson 
  #     WHERE comm BETWEEN 0.12 and 0.14;

################################################################
# (II) Three simple SQL queries, each with a logical operator 
# (AND, OR, NOT ) to combine two logic conditions involving two 
# relational operators in the where clause
################################################################
#SELECT *
 #    FROM salesperson
  #       WHERE city LIKE '%on%' OR comm >= 0.13;

#SELECT *
 #    FROM salesperson
  #        WHERE city LIKE '%on%' AND comm >= 0.13;

#SELECT *
 #    FROM salesperson
  #       WHERE NOT (city LIKE '%on%' AND comm >= 0.13);

CREATE TABLE AGENT(A_number INTEGER UNIQUE PRIMARY KEY, A_name VARCHAR(15), A_city VARCHAR(15), A_comm DECIMAL(3, 2));
INSERT INTO AGENT VALUES 
	(1001, 'Joshua', 'London', 0.12),
    (1002, 'Mary', 'San Jose', 0.13),
    (1004, 'Joseph', 'London', 0.11),
    (1007, 'Ruth', 'Los Angeles', 0.15),
    (1003, 'Anna', 'New York', 0.10);
    
CREATE TABLE CUSTOMER(C_number INTEGER UNIQUE PRIMARY KEY, C_name VARCHAR(15), C_city VARCHAR(15),
 C_rating INTEGER, A_number INTEGER REFERENCES AGENT(A_number));
INSERT INTO CUSTOMER VALUES
	(2001, 'John', 'London', 100, 1001),
    (2002, 'Peter', 'Los Angeles', 200, 1003),
    (2003, 'Paul', 'San Jose', 200, 1002),
    (2004, 'Solomon', 'New York', 300, 1002),
    (2006, 'David', 'London', NULL, 1001),
    (2008, 'Cisneros', 	'San Jose', 300, 1007),
    (2007, 'Pereira', 'Rome', 100, 1004);
    
CREATE TABLE ORDER_LINE(O_number INTEGER UNIQUE PRIMARY KEY, O_amount DECIMAL(8,2), O_date DATE,
 C_number INTEGER REFERENCES CUSTOMER (C_number), A_number INTEGER REFERENCES AGENT(A_number));
 
INSERT INTO ORDER_LINE VALUES
	(3001, 18.69, '2000-10-03', 2008, 1007), 
    (3003, 767.19, '2000-10-03', 2001, 1001),
    (3002, 1900.10, '2000-10-03', 2007, 1004),
    (3005, 5160.45, '2000-10-03', 2003, 1002),
    (3006, 1098.16, '2000-10-03', 2008, 1007),
    (3009, 1713.23, '2000-10-04', 2002, 1003),
    (3007, 75.75, '2000-10-04', 2004, 1002),
    (3008, 4723.00, '2000-10-05', 2006, 1001),
    (3000, 1309.95, '2000-10-06', 2004, 1002),
    (3011, 9891.88, '2000-10-06', 2006, 1001);

SHOW DATABASES;
SHOW TABLES;

SELECT * FROM CUSTOMER WHERE C_city='Los Angeles' AND C_rating > 150;
SELECT * FROM CUSTOMER WHERE (C_city LIKE '%n%' AND C_name LIKE '%n%'); # Note it's case insensitive!
SELECT * FROM CUSTOMER WHERE C_city LIKE BINARY('%n%') AND C_name LIKE BINARY('%n%');
