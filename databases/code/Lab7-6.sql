USE cs364;

-- SHOW TABLES;

-- DESCRIBE CUSTOMER;
SELECT AVG(C_rating) FROM CUSTOMER;

SELECT * FROM CUSTOMER ORDER BY C_rating ASC;

SELECT C_city, AVG(C_rating) FROM CUSTOMER GROUP BY C_city;

SELECT DISTINCT COUNT(*) FROM SALESPERSON;

SELECT COUNT(DISTINCT city) FROM SALESPERSON;

SELECT sname, snum, comm from salesperson where comm=(select max(comm) from salesperson);

SELECT sname, snum, comm from salesperson where comm=(select min(comm) from salesperson);

SELECT sname, snum, comm from salesperson where comm>(select avg(comm) from salesperson);

show databases;

use saleco;

#describe product;

#select * from product;
#select * from vendor;

select P_DESCRIPT, P_PRICE, PRODUCT.V_CODE, V_NAME, V_CONTACT, V_AREACODE, V_PHONE from product, vendor
 where product.v_code = vendor.v_code 
 GROUP BY P_DESCRIPT;
 
select P_DESCRIPT, P_PRICE, PRODUCT.V_CODE, V_NAME, V_CONTACT, V_AREACODE, V_PHONE from product, vendor 
where product.v_code = vendor.v_code 
GROUP BY P_DESCRIPT
ORDER BY P_PRICE;

select P_DESCRIPT, P_PRICE, PRODUCT.V_CODE, V_NAME, V_CONTACT, V_AREACODE, V_PHONE from product, vendor 
where product.v_code = vendor.v_code and P_INDATE > '2010-01-15';

select * from invoice;
select * from customer;

SELECT * FROM INVOICE WHERE CUS_CODE = 10014;

SELECT INV_NUMBER as invoice_number, CUS_CODE as customer_code, INV_DATE as invoice_date 
FROM INVOICE 
WHERE CUS_CODE = 10014;


