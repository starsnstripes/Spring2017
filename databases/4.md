# Lesson 4
## Relational Tables
1. A table is a 2d struct
2. each table row (tupl) represents a single entiy occurence within the entity set
3. Each table column represents an attr and each column has a distinct name


## Keys
+ Superkeys are an attribute or combination of attributes that uniquely identifies each row in a table
  + STUDENT_NUM could be a superkey, or the entire row (technically)
+ Candidate keys are minimal superkeys aka a superkey that does not contain a subset of attributes that is itself a superkey
  + Can't be NULL
+ Primary keys are candidate keys selected to uniquely identify all other attribute values in any given row; cannot contain null entries
  + Entity Integrity (requirement) is conserved here.
  + **Primary Keys can be more than one field (whatever minimum attrs necessary to make a row unique)**

+ Foreign keys are an attribute or comb of attributes that are either null or point to another table's primary key.

## Operators
+ Natural Join - 3 step process defined [here](https://www.w3resource.com/mysql/advance-query-in-mysql/mysql-natural-join.php).
+ Left outer joins line up a table with one key with another table that has an equivalent primary key

## Changing the M:N relationship to two 1:M relationships.
+ For when a student has lots of courses. For instance there would be an ENROLLMENT table between the STUD table and CLASS table.
