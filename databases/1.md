# Lesson 1

## General Notes
+ This class will be pretty slide-heavy.
+ Lots of lectures and notes.
+ This class is something that Col Harder has told you is *hard*.
+ No final exam in the class, but there's a final project.

## Content
+ Design of Databases: E/P Model, Relational Model, Semistructured Model
  + Includes "SQL" and "NoSQL" styles of databases.
+ Database Programming
+ Web Programming: HTML, CSS, PHP, JavaScript, XML, XSLT, XPath, XQuery, AJAX
+ DB/Web Security
+ *not* DBA - whatever that means.

## Why?
+ **Database systems are at the core of CS**
+ Modern shift from computation to information.
+ Needs to maintain data schematics
