# Course Admin and Prof/UnProf Relationships
### What is PFP?
+ 9 sorties (13.5 flight hours) and the chance to solo

### Contact info:
![PFP Contact Info](images/contact.PNG)

### No-Shows
+ First time is a warning, second time is a Form 10, 3rd time is a definite no solo and possible removal from AM-420

### Required Items
![REQ Items](images/reqitems.PNG)

## Scheduling
+ You will be notified if there is no class - **always assume there is otherwise**
+ Schedule emailed daily - *but* be ready to fly any day as you will replace cadets that aren't flying that day.

### Bus Schedule
![Bus Schedule](images/bus.PNG)
**Cannot drive POV's to AM420**
You never need to go to the airfield on changed SOC and weather CW delays unless otherwise told.

## Flight Duty Rq's
+ 12 hours bottle-to-throttle - can't be hungover.
+ ONLY Tylenol, Motrin, and Asprin are approved for acute care - everything else is an automatic DNIF
+ Giving Blood is a 72-hour DNIF
+ Flight Duty begins when aircrew reports for mission briefings or other official duty. Ends when your engine shuts down on the final flight of the day.

## Crew Rest
+ Must have a min of 12 non-duty hours prior to first military duty of the day.
+ Must have an *opportunity* for eight hours of sleep.
+ You can study and reply to emails during crew rests, but no briefings.
+ You **cannot** go to anything compulsory.
+ Crew rest does **not** interfere with urinalysis

## Quizzes
#### Emergency Procedure Quizzes
+ Two 15-question quizzes, open book and studying with classmates is encouraged.
+ Cadets need to pass both with a minimum of 85 (correctible to 100)
+ Final exam to be passed before solo/nav sorties with a minimum score of 85% on 30 questions. Need to pass in two or less attempts.
+ Check appendix A of the playbook in the k drive

### Cool Traditions:
+ First ride is a "dollar ride" - customized dollar w/ quotes from a first flight where the IP demos most of the stuff.
+ Dunk tank if you solo.

### Solo REq's
+ All gradeable maneuvers except chandelle and lazy 8) must meet syllabus requirements.
+ Must also get a recommendation.
+ 7 day max between F207 ride and solo date.
