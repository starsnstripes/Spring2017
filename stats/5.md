# Bayes' Rule
[Reference](https://en.wikipedia.org/wiki/Bayes%27_theorem)
+ Syntax of the question is "Given <this thing>, what is the probability of <a different thing> happening?"

# Law of total Probability
[Reference](https://en.wikipedia.org/wiki/Law_of_total_probability)
